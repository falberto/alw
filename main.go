package main

import (
	"os"
    "strconv"  
    "fmt"
    "bufio"
)


func userInterface(bio *bufio.Reader)(int64){

    fmt.Println(" ------------------------------------------------------------"); 
    fmt.Println(" --                                                        --");
    fmt.Println(" --  OPERATIONS                                            --");
    fmt.Println(" --                                                        --");
    fmt.Println(" --  0) Load Another Dataset (File OSM's Path)             --");
    fmt.Println(" --  1) Geocoder                                           --");
    fmt.Println(" --                                                        --");
    fmt.Println(" ------------------------------------------------------------");
    fmt.Println("");
    
    var op int64
    var err error

    for{
            fmt.Print(" Select Operation:  ");
            operation, _, errOperation := bio.ReadLine()
            check(errOperation)


            op, err = strconv.ParseInt(string(operation), 10, 10)
    
            if (err != nil) || (!(op == 0 || op == 1)){
                fmt.Println(" [ERROR] Operation is not valid! (only 0 (Load Dataset) or 1 (Geocoder))")
                fmt.Println("")
            } else{ break
            }
    }

    return op
}


func main(){

    bio := bufio.NewReader(os.Stdin)

    fmt.Println("");
    fmt.Print(" Insert the database's name:  ");
    databaseName, _, err := bio.ReadLine()
    check(err)

    existDB,db := connectDB(string(databaseName))

    if !existDB{
        fmt.Println(" [ERROR] The selected POSTGRES/POSTGIS Database isn't created previously into system\n")
        os.Exit(1)
    }
 
    defer db.Close()

    existTableSchema := checkTablesSchema(db)
    
    if !existTableSchema{
        createDBTable(db)
        loadDataset(db,bio)    
    }

    fmt.Println("\n ------------------------------------------------------------");
    fmt.Println(" --                                                        --");
    fmt.Println(" --          ALW Project             A.A. 2016/2017        --");
    fmt.Println(" --                                                        --");
    fmt.Println(" --                                                        --");
    fmt.Println(" --  VERSION                                               --");
    fmt.Println(" --             2.1                                        --");
    fmt.Println(" --                                                        --");
    fmt.Println(" --  AUTHORS                                               --");
    fmt.Println(" --             Fabio Alberto Coira                        --");
    fmt.Println(" --             Matteo Santangeli                          --");
    fmt.Println(" --             Davide Nanni                               --");
    fmt.Println(" --                                                        --");
    fmt.Println(" ------------------------------------------------------------");
    
    firstRun := true

    for{
    
        if !firstRun{
            fmt.Println("");
            fmt.Println("");    
            fmt.Println("");
            fmt.Println("");
        }
        
        op := userInterface(bio)

        switch op {
                            /**
                                    see "parserOSM2.go"
                             **/
            case 0 : loadDataset(db,bio)

                            /**
                                    see "geocoder.go"
                             **/
            case 1 : geocoder(db,bio)
        }

        fmt.Println("");

        firstRun = false
    }
}
