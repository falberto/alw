package main

import (
	"fmt"
	"github.com/glaslos/go-osm"
    "strings" 
 	"database/sql"
    "os"
    "io/ioutil"
    "bufio"
    "regexp"
    "bytes"
    "strconv"
    "gopkg.in/cheggaaa/pb.v1"
)


func loadDataset(db *sql.DB,bio *bufio.Reader){

    var dataset string

    for {
        fmt.Println("");
        fmt.Print(" Insert OSM File's Dataset PATH: ")
        pathOSMFile, _, err := bio.ReadLine()
        check(err)
    
        dat, errDat := ioutil.ReadFile(string(pathOSMFile)) 
        
        if errDat != nil{
            fmt.Println(" [ERROR] The OSM File's Dataset PATH is not valid!")
        } else {
            dataset = string(dat) 
        break
        }
    }

        //  Parse the selected OSM file and insert value into tables
    parserFileOSM(db,dataset)

    fmt.Println("\n [LOAD DATASET]")
}


func parserFileOSM(db *sql.DB,dat string){

    m, _ := osm.DecodeString(dat)

    fmt.Println("\n [  SAVING NODES INFO ------------------------------------  ]  ")
    insertNodes(db,m)
 
    fmt.Println("\n [  SAVING WAYS INFO  ------------------------------------  ]")
    insertWays(db,m)
      
    fmt.Println("\n [  SAVING RELATIONS INFO --------------------------------  ]")
    insertRelations(db,m)
    fmt.Println("\n [  COMPLETED SAVING RELATIONS INFO ----------------------  ]")

    fmt.Println("\n [  FIXING WAYS STREET FROM RELATION TABLE   -------------  ]")
    updateRelationsWays(db)
    
    fmt.Println("\n [  UPDATING WAYS TABLE WITH POLYLINES OBJs --------------  ]")
    initPolylines(db)
    
    fmt.Println("\n [  FIXING NODES, WAYS TABLES   --------------------------  ]")
    fixNodesWaysPolylines(db)
    
    fmt.Println("\n [  SAVING RELATIONS's CENTROIDS  ------------------------  ]")
    updateCentroidRelations(db)
	
	fmt.Println("\n [  UPDATING WAYS  ---------------------------------------  ]")
	updateWaysCities(db)
	
	fmt.Println("\n [  UPDATING RELATIONS  ----------------------------------  ]")
	updateRelationsCities(db)
}


type ListCityNotNull struct {
    geom string
    city string
}

                        /**********************************************************************
                         *  
                         *  NODES Entity: init table operations 
                         *
                         **********************************************************************/
func insertNodes(db *sql.DB,m *osm.Map){
 
    numNodes := len(m.Nodes) 

	if numNodes == 0 {
    	fmt.Println("\n [ERROR INSERTING NODES] you didn't type add")
        os.Exit(1)
    }

    var housenumber, city, street string

	bar := pb.StartNew(numNodes)
    bar.ShowBar = false

	for i, _ := range m.Nodes {
		 
        housenumber = ""
        city = ""
        street = ""

		for _, tag := range m.Nodes[i].Tag {
			
            if(tag.Key == "addr:housenumber"){
                housenumber = getHousenumber(tag.Value)
            }

			if(tag.Key == "addr:city"){
				city =  tag.Value
			}


			if(tag.Key == "addr:street"){
				street =  tag.Value
			}    
		}

        if street != "" {
            street = "\""+street+"\""
        }

        if city != "" {
            city = "\""+city+"\""
        }

        id := m.Nodes[i].Elem.ID
        lng := m.Nodes[i].Lng
        lat := m.Nodes[i].Lat

        bar.Increment()

        saveNodeInfo(db,id,lng,lat,street,housenumber,city)
	}

    bar.FinishPrint(" [  COMPLETED SAVING NODES INFO --------------------------  ]  ")
}

func getHousenumber(rawString string)(string){

    re := regexp.MustCompile("[0-9]+")

    valueHM := re.FindAllString(rawString, -1)

    var housenumbers bytes.Buffer

    for i,str := range valueHM{

        housenumbers.WriteString(str)
    
        if (i < len(valueHM)-1){
            housenumbers.WriteString("-")}
    }

    return housenumbers.String()
}


func saveNodeInfo(db *sql.DB, id int64, lng, lat float64, street, housenumbers, city string){

    re := regexp.MustCompile("-")

    valueHM := re.Split(housenumbers, -1)

    for _,str := range valueHM{
    
        sqlStatement := ""

        if str != ""{

            valueIntegerHM,errInt := strconv.ParseInt(valueHM[0],10,64)
            check(errInt)

            sqlStatement = `INSERT INTO nodes (id, geom, housenumber, street, city) VALUES ($1, ST_SetSRID(ST_MakePoint($2, $3), 4326), $4, $5, $6)`
            
            _, err := db.Exec(sqlStatement, id, lng, lat, valueIntegerHM, street, city)
		    check(err)
        } else {
            sqlStatement = `INSERT INTO nodes (id, geom, street, city) VALUES ($1, ST_SetSRID(ST_MakePoint($2, $3), 4326), $4, $5)`            

            _, err := db.Exec(sqlStatement, id, lng, lat, street, city)
		    check(err)
        }
    }
}

                        /**********************************************************************
                         *  
                         *  WAYS Entity: init table operations 
                         *
                         **********************************************************************/
func initPolylines(db *sql.DB){

            /**
                create polyline from a set of points geometry objects
             **/
    sqlStatementLinestring := "select st_astext(st_makeline(geom)),wayID from (select idnodes,geom,wayID,tmp3.street from nodes inner join (select idnodes,wayID,street from includes inner join (select id as wayID, street from ways where street<>'') as tmp on idways=wayID) as tmp3 on idnodes=id) as tmp4 group by wayID having count(*)>=2"


    rowsLinestring,errLinestring := db.Query(sqlStatementLinestring)
    check(errLinestring)

    for rowsLinestring.Next(){
        var geom,wayID string

        err := rowsLinestring.Scan(&geom,&wayID)
        check(err)        

        sqlStatementUpdateWaysTable := "UPDATE ways SET geom=st_geometryfromtext('"+geom+"') WHERE id='"+wayID+"'"

        execSQLQuery(db,sqlStatementUpdateWaysTable)
    }
}


func insertWays(db *sql.DB,m *osm.Map){

    numWays := len(m.Ways)

	if numWays == 0 {
    	fmt.Println("\n [ERROR INSERTING WAYS] you didn't type add")
        os.Exit(1)
    }

	bar := pb.StartNew(numWays)
    bar.ShowBar = false

    for i, _ := range m.Ways {
        
        id := m.Ways[i].Elem.ID

        for _, node := range m.Ways[i].Nds {

            sqlStatement := `INSERT INTO includes (idnodes, idways) VALUES ($1, $2)`

		    _, err := db.Exec(sqlStatement, node.ID, id)
		    check(err)
        }

        var /*housenumber,*/ city, street string
		 
        //housenumber = ""
        city = ""
        street = ""

		for _, tag := range m.Ways[i].RTags {

			if(tag.Key == "is_in:city"){
				city =  tag.Value
			}


			if(tag.Key == "name"){
				street =  tag.Value
			}    
		}

        if street != "" {
            street = "\""+street+"\""
        }

        if city != "" {
            city = "\""+city+"\""
        }

        bar.Increment()

        sqlStatement := `INSERT INTO ways (id, street, city) VALUES ($1, $2, $3)`

		_, err := db.Exec(sqlStatement, id, street, city)
		check(err)
    }

    bar.FinishPrint(" [  COMPLETED SAVING WAYS INFO ---------------------------  ]  ")
}



                        /**********************************************************************
                         *  
                         *  RELATIONS Entity: init table operations
                         *
                         **********************************************************************/
func readBuildingTagsValues(i int,m *osm.Map)(bool,string,string,string,string){

        housenumber := ""
        city := ""
        street := ""
        name := ""

        isbuilding := false

        for _, tag := range m.Relations[i].Tags {

            if(tag.Key == "addr:housenumber"){
                housenumber = getHousenumber(tag.Value)
            }

			if(tag.Key == "addr:city"){
				city =  tag.Value
			}


			if(tag.Key == "addr:street"){
				street =  tag.Value
			}

			if(tag.Key == "name"){
				name =  tag.Value
			}
    
			if(tag.Key == "building"){
				isbuilding = true
			}
		}

        if street != "" {
            street = "\""+street+"\""
        }

        if city != "" {
            city = "\""+city+"\""
        }

        if name != "" {
            name = "\""+name+"\""
        }

        return isbuilding,street,name,housenumber,city
}


func saveRelInfo(db *sql.DB, id int64, street, housenumbers, city string){

    re := regexp.MustCompile("-")

    valueHM := re.Split(housenumbers, -1)

    for _,str := range valueHM{
    
        sqlStatement := ""

        if str != ""{

            valueIntegerHM,errInt := strconv.ParseInt(valueHM[0],10,64)
            check(errInt)

            sqlStatement = `INSERT INTO relations (id, housenumber, street, city) VALUES ($1, $2, $3, $4)`
            
            _, err := db.Exec(sqlStatement, id, valueIntegerHM, street, city)
		    check(err)
        } else {
            sqlStatement = `INSERT INTO relations (id, street, city) VALUES ($1, $2, $3)`            

            _, err := db.Exec(sqlStatement, id, street, city)
		    check(err)
        }
    }
}




func saveBuilding(db *sql.DB,m *osm.Map, i int, id int64, street, housenumber, city string){

        saveRelInfo(db,id,street,housenumber,city)

        for _, member := range m.Relations[i].Members {

            if member.Type == "way"{

                sqlStatement := `INSERT INTO relationsWays (idway,idrelation) VALUES ($1, $2)`

		        _, err := db.Exec(sqlStatement, member.Ref, id)
		        check(err)
            }

            if member.Type == "node"{

                sqlStatement := `INSERT INTO relationsNodes (idnodes, idrelation) VALUES ($1, $2)`

		        _, err := db.Exec(sqlStatement, member.Ref, id)
		        check(err)
            }
        }

}

func insertRelations(db *sql.DB,m *osm.Map){

    numRelations := len(m.Relations)

	if numRelations == 0 {
    	fmt.Println("\n [ERROR INSERTING RELATIONS] you didn't type add")
        os.Exit(1)
    }

    for i, _ := range m.Relations {
        
        id := m.Relations[i].Elem.ID

                /**
                    read only the building's street,city,housenumber values
                 **/    
        isbuilding,street,_,housenumber,city := readBuildingTagsValues(i,m) 

                /**
                    save building's info into relations table
                 **/   
        if isbuilding && street!=""{
                
            saveBuilding(db,m,i,id,street,housenumber,city) 
        } 
    }  
}




                        /**********************************************************************
                         *  
                         *  UPDATE SCHEMA operations
                         *
                         **********************************************************************/
            /**
                update the street into ways table from relations table
             **/
func updateRelationsWays(db *sql.DB){

    sqlStatementIDRelations := "select id,street,city from relations"

    rows,err := db.Query(sqlStatementIDRelations)
    check(err)
    
    for rows.Next(){
        var id,street,city string

        err = rows.Scan(&id,&street,&city)
        check(err)

        sqlStatementUpdateWays := ""

        if !(city == ""){
            sqlStatementUpdateWays = "update ways set street='"+street+"',city='"+city+"' where id in (select ways.id from ways inner join relationsways on ways.id=relationsways.idway where idrelation='"+id+"')" 
        } else {
            sqlStatementUpdateWays = "update ways set street='"+street+"' where id in (select ways.id from ways inner join relationsways on ways.id=relationsways.idway where idrelation='"+id+"')"
        }

        execSQLQuery(db,sqlStatementUpdateWays)
    }
}

            /**
                insert the relation's centroid value into relations geom column 
             **/
func updateCentroidRelations(db *sql.DB){

    sqlStatementDistinctIDRelation := "select distinct(idrelation) from relationsways"

    rows,err := db.Query(sqlStatementDistinctIDRelation)
    check(err)

    for rows.Next(){
        var idrelation string

        err = rows.Scan(&idrelation)
        check(err)

        sqlStatementMultipointWays := "select st_setsrid(st_union(centroid),4326) as multipoint from (select st_centroid(ways.geom) as centroid from ways inner join relationsways on ways.id=idway where idrelation='"+idrelation+"' and ways.geom<>'') as tmp"

        sqlStatementMultipointNodes := "select st_union(nodesGeom) as multipoint from (select nodes.geom as nodesGeom from nodes inner join relationsnodes on nodes.id=relationsnodes.idnodes where idrelation='"+idrelation+"' and nodes.geom<>'') as tmp2"

        sqlStatementMultipoint := "("+sqlStatementMultipointWays+") UNION ("+sqlStatementMultipointNodes+")"
 
        sqlStatementMultipointCentroid := "select st_centroid(multipoint) from ("+sqlStatementMultipoint+") as tmp3 where multipoint<>''"       
        
        sqlStatementUpdateCentroidRelations := "update relations set geom=("+sqlStatementMultipointCentroid+") where id='"+idrelation+"'"               

        execSQLQuery(db,sqlStatementUpdateCentroidRelations)       
    }
}


            /**
                FIX NODES AND WAYS TABLES
             **/
func fixNodesWaysPolylines(db *sql.DB){
                

                    /**
                        FIRST PHASE: Fix ways
                     **/
    fixWaysTable(db)
                    
                    /**
                        SECOND PHASE: Fix city into nodes from ways where nodes.id are into relation with ways by includes table
                     **/
    sqlStatementFixNodesTable := "update nodes set city=(select distinct(city) from ways where city<>'') where nodes.id in(select nodes.id from nodes inner join includes on nodes.id=includes.idnodes inner join ways on includes.idways=ways.id where ways.city<>'')"

    execSQLQuery(db,sqlStatementFixNodesTable)
}



func fixWaysTable(db *sql.DB){
            
                /**
                    get the list of street which have more than 2 city column not null into ways table
                 **/        
    sqlQueryStreetList := "select street from (select street,count(*) as numStreet from ways where street<>'' and city<>'' group by street) as tmp where numStreet>=2" 

	rows, err := db.Query(sqlQueryStreetList)
    check(err)    

    for rows.Next(){

        var street,city string
    
        err = rows.Scan(&street)
        check(err)

        sqlQueryStreet := "select city from ways where street IN ("+sqlQueryStreetList+") and city<>'' group by city"
	    rowsCities, errCities := db.Query(sqlQueryStreet)
        check(errCities)  
        
        i := 0        
                /**
                    check if a street has got polylines in multiple cities
                 **/ 
        for rowsCities.Next(){        
            err = rowsCities.Scan(&city)
            check(err)

            i++
        }
                /**
                    if the selected street has got polylines into the same street,
                        update the ways table and set the city column of the street rows
                        with the last city value
                 **/
        if i == 1{
            street = strings.Replace(street, "'", "''", -1)
            sqlQueryUpdate := "update ways set city='"+city+"' where street~'"+street+"'"       
            execSQLQuery(db,sqlQueryUpdate)
        }

    }
}
/** UPDATE RELATIONS TABLE ADDING CITIES**/
func updateRelationsCities(db *sql.DB){
	
	//sqlStatementUpdateRelationsCities := "update relations set city=(select distinct(city) from nodes where city<>'') where relations.id in(select relations.id from relations inner join relationsnodes on relations.id=relationsnodes.idrelation inner join nodes on relationsnodes.idnodes = nodes.id where nodes.city <>'')"

	sqlStatementSelectNodesCity := "select relations.id,nodes.id,nodes.city from relations inner join relationsnodes on relations.id=relationsnodes.idrelation inner join nodes on relationsnodes.idnodes = nodes.id"
	
	rows, err := db.Query(sqlStatementSelectNodesCity)
    check(err)  
	
	for rows.Next(){

        var relationId,nodesId,nodesCity string
    
        err = rows.Scan(&relationId,&nodesId,&nodesCity)
        check(err)

        sqlQueryRelationsCity := "update relations set city ='"+nodesCity+"' where id ='"+relationId+"' "
	    
        execSQLQuery(db,sqlQueryRelationsCity)
    }
}

/** UPDATE WAYS TABLE ADDING CITIES**/
func updateWaysCities(db *sql.DB){
	
	//sqlStatementUpdateWaysCities := "update ways set city=(select distinct(city) from nodes where city<>'') where ways.id in(select ways.id from nodes inner join includes on nodes.id=includes.idnodes inner join ways on includes.idways = ways.id)"

	sqlStatementSelectNodesCity := "select ways.id,nodes.id,nodes.city from nodes inner join includes on nodes.id=includes.idnodes inner join ways on includes.idways = ways.id"
	
	rows, err := db.Query(sqlStatementSelectNodesCity)
    check(err)  
	
	for rows.Next(){

        var waysId,nodesId,nodesCity string
    
        err = rows.Scan(&waysId,&nodesId,&nodesCity)
        check(err)

        sqlQueryWaysCity := "update ways set city ='"+nodesCity+"' where id ='"+waysId+"' "
	    
        execSQLQuery(db,sqlQueryWaysCity)

    }
}
