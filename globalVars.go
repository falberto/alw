package main

import (
	"fmt"
	"database/sql"
    _"github.com/lib/pq"
)

const (  
  host     = "localhost"
  port     = 5432
  user     = "postgres"
  password = "postgres"
)

                        /*
                         *  connect to a Postgres/Postgis database
                         */
func connectDB(dbname string)(bool,*sql.DB){

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+ "password=%s dbname=%s sslmode=disable",  host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)    	
    check(err)

    sqlStatement := "SELECT 1 AS result FROM pg_database WHERE datname~'"+dbname+"'";   

    _,errRows := db.Query(sqlStatement)
    
    if errRows != nil {
        return false,nil
    } else {
        return true,db
    }
}

                        /*
                         *  exec the postgresql/postgis insert query
                         */
func execSQLQuery(db *sql.DB, query string){
    _, err := db.Exec(query)

    check(err)
}



func check(e error) {
    if e != nil {
        panic(e)
    }
}
