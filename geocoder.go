package main

import (
	"fmt"
	"database/sql"
    "strings"
    "bufio"
    "strconv"
    "regexp"  
)

func geocoder(db *sql.DB,bio *bufio.Reader){

    fmt.Println("");    
    fmt.Println("");
    fmt.Println("");
    fmt.Println("\n [   ----------------   GEOCODER  MODE   -----------------  ]")

    for{
        fmt.Println("\n\n Insert Address <street,housenumber,city> (\"q\" or \"e\" to exit)")
        fmt.Print(" > ")
        readAddress, _, err := bio.ReadLine()
        check(err)

        address := string(readAddress)        

        if address == "e" || address == "q"{
            break
        }

        errValue, street, housenumber, city := checkNormalizedFormatAddress(address)
            
        if errValue == -1{
            fmt.Println("\n   [ERROR] The address format is not normalized <street,housenumber,city>!")
            continue
        }

            //  replace all "'" characters with "''"
        street = strings.Replace(street, "'", "''", -1)

        // Case1: Match esatto con un nodo
        _, longitude, latitude := exactMatching(db, street, housenumber, city)
        if(longitude != -1 && latitude != -1){

            fmt.Printf("\n   | CASE 1:  EXACT MATCHING!"+
                       "\n   | ")
            fmt.Printf("\n   | [Longitude]       %f\n   | [Latitude]        %f \n", longitude, latitude)
            continue
        }

        // Case2: Check if street exists
        if(!streetExists(db, street)){
            fmt.Printf("\n   | CASE 2: STREET IS NOT IN DB!"+
                       "\n   | ")
            fmt.Printf("\n   |      There isn't any record in db that matches\n")
            continue
        }

        //ricerca nelle ways, tutti i nodi che hanno un civico...
            // - non c' è nemmeno un civico -> torno coordinate intervallo, [x1, y1], [x2, y2] 
            // - ci sono almeno due civici, uno più piccolo e uno più grande più vicini [x1, y1], [x2, y2] 
            // - ci sono gli intervalli aperti, se è più piccolo interpoliamo, se è più grande di poco ok, se è troppo grande no
        searchHouseNumberFromWay(db, street, housenumber);
    }

    fmt.Println("\n [   -------------   EXIT GEOCODER  MODE   ---------------  ]")
}


                        /*
                         *  check if the inserted address observes the normalized format 
                         *
                         *       (e.g. "Via Casilina,20,Roma")
                         */
func checkNormalizedFormatAddress(address string)(int,string,string,string){
    
    splittedAddress := strings.Split(address,",")

    errValue := -1
    street := ""
    city := ""
    housenumber := ""

    re := regexp.MustCompile("[0-9]+")

    if len(splittedAddress) == 3{
        errValue = 0
        street = splittedAddress[0]
        city = splittedAddress[2]

        housenumber = re.FindAllString(splittedAddress[1], -1)[0]
    }
    
    return errValue, street, housenumber, city 
}



/********************************************************************************

                EXACT MATCHING

                    return the exact longitude and latitude values ( if exist )
                        from normalized address (e.g. "Via Casilina,20,Roma")

*********************************************************************************/

func exactMatching(db *sql.DB, street string, housenumber string, city string)(int,float64,float64){

    sqlStatement := 
    "(SELECT geom FROM nodes WHERE (nodes.street~'\""+street+"\"' AND nodes.housenumber="+housenumber+" AND nodes.city~'\""+city+"\"'))"+
    " UNION "+
    "(SELECT geom FROM relations WHERE (relations.street~'\""+street+"\"' AND relations.housenumber="+housenumber+" AND relations.city~'\""+city+"\"'))"

    rows, err := db.Query(sqlStatement)
	check(err)

    if !(rows.Next()){

        sqlStatement = "SELECT geom FROM nodes WHERE street~'\""+street+"\"' AND housenumber="+housenumber+"";

        rowsGeom, errGeom := db.Query(sqlStatement)
	    check(errGeom)

        for rowsGeom.Next(){
            var geom string

            err = rowsGeom.Scan(&geom)
            check(err)        

            sqlStatementListCities := "SELECT city,count(*) as numCity FROM nodes "+
                                      "WHERE ST_Distance(geom::geography, ('"+geom+"')::geography) <= 50 GROUP BY city HAVING city<>''"

            rowsDistinctCity, errDistinctCity := db.Query(sqlStatementListCities)
            check(errDistinctCity)

            var city string
            var maxCity int64
        
            maxCity = 0
        
            i := 0

            for rowsDistinctCity.Next(){
            
                var tmpCity, numCities string
            
                err = rowsDistinctCity.Scan(&tmpCity,&numCities)
                check(err)

                numCitiesInt, errNumCitiesInt := strconv.ParseInt(numCities,10,64)
                check(errNumCitiesInt)

                if ( numCitiesInt > maxCity ){
                    maxCity = numCitiesInt
                    city = tmpCity
                }
                
                i++
            }

            if i==1{
                sqlStatementUpdateNodesTable := "UPDATE nodes SET city='"+city+"' WHERE  ST_Distance(geom::geography, ('"+geom+"')::geography) <= 50"
                execSQLQuery(db,sqlStatementUpdateNodesTable)

                longitude,latitude := calcAvgLongitudeLatitude(db,sqlStatement)

                return 0,longitude,latitude
            }
        }
         
        return -1,-1,-1
    }else{
        longitude,latitude := calcAvgLongitudeLatitude(db,sqlStatement)

        return 0,longitude,latitude
    }
}


func calcAvgLongitudeLatitude(db *sql.DB,sqlStatement string)(float64,float64){
                /**
                    may return more than one result
                 **/
        var longitude, latitude float64        

        sqlStatement = "SELECT ST_X(geom) as longitude, ST_Y(geom) as latitude FROM ("+sqlStatement+") AS tmp"

        sqlStatement = "SELECT avg(longitude),avg(latitude) FROM ("+sqlStatement+") as tmp2"

        rows, err := db.Query(sqlStatement)
	    check(err)

        rows.Next()
        err = rows.Scan(&longitude, &latitude)
        check(err)
        
        return longitude,latitude
}


/********************************************************************************

                check if a street in into the "ways" or "nodes" table

*********************************************************************************/

func streetExists(db *sql.DB, street string)(bool){

    sqlStatement := "(SELECT id FROM nodes WHERE street~'\""+street+"\"') UNION "+
                    "(SELECT id FROM ways WHERE street~'\""+street+"\"') UNION "+
                    "(SELECT id FROM relations WHERE street~'\""+street+"\"')"

    rows, err := db.Query(sqlStatement)
    check(err)

    if !(rows.Next()){
        return false
    }else{
        return true
    }
}



/*******************************************

                POLYLINES

*******************************************/

            /**
                get the lng_startpoint,lat_startpoint,lng_endpoint,lat_endpoint
             **/
func getStartEndPointPolyline(db *sql.DB,street string){

    startpointWay,lng_startpoint,lat_startpoint := getPoint(db,"st_startpoint(ways.geom)",street)

    if startpointWay{
        _,lng_endpoint,lat_endpoint := getPoint(db,"st_endpoint(ways.geom)",street)

            fmt.Printf("\n   | CASE 2: STREET IS NOT IN DB! (POLYLINES)"+
                       "\n   | "+
                       "\n   |      There are at least two points about this street without housenumbers"+
                       "\n   |          The selected housenumber will be in the following range\n   | ")
            fmt.Printf("\n   |      STARTPOINT  [Longitude]       %s     [Latitude]        %s\n   |", lng_startpoint,lat_startpoint)
            fmt.Printf("\n   |      ENDPOINT    [Longitude]       %s     [Latitude]        %s \n", lng_endpoint,lat_endpoint)          
    } else {
            fmt.Printf("\n   | CASE 2: STREET IS NOT IN DB!"+
                       "\n   | ")
            fmt.Printf("\n   |      There is only one point about this street without housenumber\n")
    }
}


            /**
                get the polyline's point lng_point,lat_point
             **/
func getPoint(db *sql.DB,interestedPoint,street string)(bool,string,string){

    var lng_point,lat_point string

    sqlQuery := "(select "+interestedPoint+" as point from ways where street~'\""+street+"\"' and st_astext(geom)<>'') UNION "+
                "(select "+interestedPoint+" as point from ways inner join relationsways on ways.id=idway inner join relations on idrelation=relations.id where relations.street~'\""+street+"\"' and st_astext(ways.geom)<>'')"

    sqlQuery = "select point FROM ("+sqlQuery+") AS tmp LIMIT 1"

    sqlQuery = "select st_x(point),st_y(point) FROM ("+sqlQuery+") as tmp2"
    rows, err := db.Query(sqlQuery)
	check(err)

    if rows.Next(){
        err = rows.Scan(&lng_point,&lat_point)
        check(err)
        return true,lng_point,lat_point
    } else {
    return false,lng_point,lat_point}
}



                        /*
                         *  find housenumbers into a street
                         */

type Element struct {
    Longitude float64
    Latitude float64
    CivicNumberString string
    CivicNumber int64
}
 
func searchHouseNumberFromWay(db *sql.DB, street, housenumber string){

    // x, y dei nodi che hanno l'housenumber 

    var elem []Element

    sqlStatement := "(SELECT ST_X(nodes.geom) as longitude, ST_Y(nodes.geom) as latitude, housenumber FROM nodes inner join includes on nodes.id = includes.idnodes inner join ways on includes.idways = ways.id where ways.street ~ '\""+street+"\"' and nodes.housenumber IS NOT NULL) UNION "+
"(SELECT ST_X(geom) as longitude, ST_Y(geom) as latitude, housenumber FROM nodes WHERE street ~'\""+street+"\"' and housenumber IS NOT NULL) UNION "+
"(SELECT ST_X(geom) as longitude, ST_Y(geom) as latitude, housenumber FROM relations WHERE street~'\""+street+"\"' and housenumber IS NOT NULL)"

        // sort by asc ordering
    sqlStatement = "SELECT longitude, latitude, housenumber FROM ("+sqlStatement+") AS tmp order by housenumber"

    rows, err := db.Query(sqlStatement)
    check(err)

    noRows := true

        //quanti civici abbiamo? qual è il civico dato in input? sulla base di questo andiamo a costruirci degli intervalli che contengono l'input
    for rows.Next(){

        var element Element
        err = rows.Scan(&(element.Longitude), &(element.Latitude), &(element.CivicNumberString))
        check(err)

        numInt, errInt := strconv.ParseInt(element.CivicNumberString, 10, 64)
        check(errInt)

        element.CivicNumber = numInt
        elem = append(elem, element)

        noRows = false
    }


    if noRows{
        //restituisco gli estremi
        getStartEndPointPolyline(db,street) 
    } else {

   //caso 1: ho un solo civico utile, housenumber < o > ?
   //se è minore faccio interpolazione tra estremo minore della ways e il punto del civico che mi ha restituito
   //se è maggiore faccio la stessa cosa ma con l'estremo maggiore della way, se è più grande di poco (max 20 civici)

   //caso 2: ho due o più civici, ma uno più piccolo e uno più grande, in questo caso faccio l'interpolazione tra gli estremi più vicini
   //vicino al mio housenumber
        minMax(elem, housenumber)
    }
}

func minMax(elem []Element, housenumber string){
    var imax int = -1 
    var imin int = -1 
    hn, err := strconv.ParseInt(housenumber, 10, 64)
    check(err)
    
    for i, e := range elem {

        if hn > e.CivicNumber {
            imin = i
        }
        if hn == e.CivicNumber{
            fmt.Printf("\n   | It is found an exact matching (the node's entry hasn't the city column)\n   |\n")   
            fmt.Printf("   | [Longitude]     %f      [Latitude]     %f\n", elem[i].Longitude, elem[i].Latitude)
           return;
        }
        if hn < e.CivicNumber {
            imax = i
            break        
        }
    }

    if(imin != -1 && imax != -1){
       fmt.Printf("\n   | Il numero civico selezionato si trova nell'intervallo:\n   |\n")   
       fmt.Printf("   | [Housenumber]     %d      [Longitude]     %f      [Latitude]     %f", elem[imin].CivicNumber, elem[imin].Longitude, elem[imin].Latitude)
        fmt.Printf("\n   | [Housenumber]     %d      [Longitude]     %f      [Latitude]     %f\n", elem[imax].CivicNumber, elem[imax].Longitude, elem[imax].Latitude)
        return
    }
    //civico > di tutti quelli presenti
    if(imin != -1 && imax == -1){
       fmt.Printf("\n   | Il numero civico selezionato è più grande di quelli presenti!"+
                  "\n   | "+
                  "\n   | Il più grande tra i civici presenti è:\n   |\n   | [Housenumber]     %d", elem[imin].CivicNumber)
       fmt.Printf("\n   | [Longitude]       %f\n   | [Latitude]        %f \n", elem[imin].Longitude, elem[imin].Latitude)
    }
    if(imin == -1 ){
       fmt.Printf("\n   | Il numero civico selezionato è più piccolo di quelli presenti!"+
                  "\n   | "+
                  "\n   | Il più piccolo tra i civici presenti è:\n   |\n   | [Housenumber]     %d", elem[imax].CivicNumber)
       fmt.Printf("\n   | [Longitude]       %f\n   | [Latitude]        %f \n", elem[imax].Longitude, elem[imax].Latitude)
    }
}
