package main

import (
	"database/sql"
    "fmt"
)

                        /**********************************************************************
                         *  
                         *  Test if all database's tables are already created
                         *
                         **********************************************************************/
func checkTable(db *sql.DB, query string)(bool){
    _, err := db.Exec(query)
    
    if err != nil{
        return false
    } else {
        return true
    }
}


func checkTablesSchema(db *sql.DB)(bool){

    sqlStatement := "select 'nodes'::regclass";
    existTable := checkTable(db,sqlStatement)

    sqlStatement = "select 'ways'::regclass";
    existTable2 := checkTable(db,sqlStatement)

    existTable = existTable && existTable2

    sqlStatement = "select 'relations'::regclass";
    existTable2 = checkTable(db,sqlStatement)

    existTable = existTable && existTable2

    sqlStatement = "select 'relationsnodes'::regclass";
    existTable2 = checkTable(db,sqlStatement)

    existTable = existTable && existTable2

    sqlStatement = "select 'relationsways'::regclass";
    existTable2 = checkTable(db,sqlStatement)

    existTable = existTable && existTable2

    sqlStatement = "select 'includes'::regclass";
    existTable2 = checkTable(db,sqlStatement)

    return existTable && existTable2
}




                        /**********************************************************************
                         *  
                         *  Create all database's tables (the tables will be empty!)
                         *
                         **********************************************************************/
func createDBTable(db *sql.DB){

    createTableNodes(db)
    fmt.Println("\n [  CREATED TABLE:     Nodes   ---------------------------  ]")

    createTableWays(db)
    fmt.Println(" [  CREATED TABLE:     Ways   ----------------------------  ]")
    fmt.Println(" [  CREATED TABLE:     Ways includes Nodes  --------------  ]")

    createTableRelations(db)
    fmt.Println(" [  CREATED TABLE:     Relations  ------------------------  ]")
    fmt.Println(" [  CREATED TABLE:     RelationsWays  --------------------  ]")
    fmt.Println(" [  CREATED TABLE:     RelationsNodes  -------------------  ]\n")
}


                        /**********************************************************************
                         *  
                         *  Create the table 'NODES'
                         *
                         **********************************************************************/
func createTableNodes(db *sql.DB){

    sqlStatement := `CREATE TABLE nodes (idnd serial, id bigint, geom GEOMETRY, housenumber int, street varchar(200), city varchar(50))`

    execSQLQuery(db, sqlStatement)
}


                        /**********************************************************************
                         *  
                         *  Create the 'WAYS' and 'INCLUDES' tables
                         *
                         **********************************************************************/
func createTableWays(db *sql.DB){

    sqlStatement := `CREATE TABLE ways (id bigint primary key, geom GEOMETRY, street varchar(200), city varchar(50))`

    execSQLQuery(db,sqlStatement)

    sqlStatementIncludes := `CREATE TABLE includes (id serial, idnodes bigint, idways bigint)`

    execSQLQuery(db,sqlStatementIncludes)
}


                        /**********************************************************************
                         *  
                         *  Create the 'RELATIONS' - 'RELATIONSWAYS' - 'RELATIONSNODES' tables
                         *
                         **********************************************************************/
func createTableRelations(db *sql.DB){

    sqlStatement := `CREATE TABLE relations (idrel serial, id bigint, geom GEOMETRY, housenumber int, street varchar(200), city varchar(50))`
    execSQLQuery(db,sqlStatement)

    sqlStatementRelationsWays := `CREATE TABLE relationsWays (id serial, idway bigint, idrelation bigint)`
    execSQLQuery(db,sqlStatementRelationsWays)

    sqlStatementRelationsNodes := `CREATE TABLE relationsNodes (id serial, idnodes bigint, idrelation bigint)`
    execSQLQuery(db,sqlStatementRelationsNodes)
}
